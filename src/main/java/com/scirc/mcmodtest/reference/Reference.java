package com.scirc.mcmodtest.reference;

public class Reference
{
    public static final String MOD_ID = "mcmodtest";
    public static final String MOD_NAME = "MC Mod Test";
    public static final String VERSION = "1.7.10-1.0";
    public static final String CLIENT_PROXY_CLASS = "com.scirc.mcmodtest.proxy.ClientProxy";
    public static final String SERVER_PROXY_CLASS = "com.scirc.mcmodtest.proxy.ServerProxy";
    public static final String GUI_FACTORY_CLASS = "com.scirc.mcmodtest.client.gui.GuiFactory";
}
