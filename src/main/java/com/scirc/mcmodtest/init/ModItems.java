package com.scirc.mcmodtest.init;

import com.scirc.mcmodtest.item.ItemMCMT;
import com.scirc.mcmodtest.item.ItemPonyHoof;
import cpw.mods.fml.common.registry.GameRegistry;

public class ModItems
{
    public static final ItemMCMT ponyHoof = new ItemPonyHoof();

    public static void init()
    {
        GameRegistry.registerItem(ponyHoof, "ponyHoof");
    }
}
